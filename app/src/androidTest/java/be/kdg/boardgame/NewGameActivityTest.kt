package be.kdg.boardgame

import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import be.kdg.boardgame.authentication.LoginActivity
import org.hamcrest.CoreMatchers.anything
import org.hamcrest.Matcher
import org.hamcrest.core.StringContains.containsString
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class NewGameActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule<LoginActivity>(
        LoginActivity::class.java
    )

    @Before
    fun setup() {
        onView(withId(R.id.etUsername))
            .perform(replaceText("testlogin@gmail.com"))
            .perform(closeSoftKeyboard())
        onView(withId(R.id.etPassword))
            .perform(replaceText("testlogin"))
            .perform(closeSoftKeyboard())
        onView(withId(R.id.btnLogin))
            .perform(click())
    }

        fun waitFor(millis: Long): ViewAction {
            return object : ViewAction {
                override fun getConstraints(): Matcher<View> {
                    return isRoot()
                }

                override fun getDescription(): String {
                    return "Wait for $millis milliseconds."
                }

                override fun perform(uiController: UiController, view: View) {
                    uiController.loopMainThreadForAtLeast(millis)
                }
            }
        }

    @Test
    fun testFriend() {
        Thread.sleep(3000)
        onView(withId(R.id.btnNewGame)).check(matches(isDisplayed()))
            .perform(ViewActions.click())
        Thread.sleep(3000)
        onView(withId(R.id.spinner2))
            .perform(click())
        onData(anything()).atPosition(3)
            .check(matches(withText(containsString("lotte"))))
    }
}
