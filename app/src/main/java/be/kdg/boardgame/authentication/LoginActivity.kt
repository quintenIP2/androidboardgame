package be.kdg.boardgame.authentication

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import be.kdg.boardgame.HomeActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.authentication.dto.SignInForm
import be.kdg.boardgame.authentication.dto.SignInHeader
import be.kdg.boardgame.authentication.dto.SignInResponse
import be.kdg.boardgame.authentication.services.AuthService
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.*

class LoginActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {
    private lateinit var sharedPref: SharedPreferences

    private lateinit var retrofit: Retrofit
    private lateinit var authService: AuthService
    private var accessToken: String? = null
    private var refreshToken: String? = null
    private var errorText: String? = null
    private var facebook: Boolean = false
    private var google: Boolean = false
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private val RC_SIGN_IN = 9000

    private var callbackManager: CallbackManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initConstants()
        updateUI(false)
        addEventHandlers()
    }

    private fun initConstants() {
        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)

        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        authService = retrofit.create(AuthService::class.java)
    }

    private fun addEventHandlers() {
        btnLogin.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                validate()
                // set error
                if (errorText != null) {
                    tvError.text = errorText
                } else {
                    tvError.text = ""
                }
            }
        }
        btnFacebookLogin.setOnClickListener {
            facebook = true
            facebookLogin()
        }
        btnGoogleLogin.setOnClickListener {
            google = true
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
            googleSingIn()
        }

        btnRegister.setOnClickListener {
            startActivity(Intent(applicationContext, RegisterActivity::class.java))
        }
    }

    private suspend fun validate() = withContext(Dispatchers.Default) {
        // create header
        val header = SignInHeader(
            resources.getString(R.string.client_id),
            resources.getString(R.string.client_secret)
        )
        Log.d("SignInheader", header.toString())

        val signInForm =
            SignInForm(etUsername.text.toString(), etPassword.text.toString())

        // send sign in request
        val body: SignInResponse?
        val response: Response<SignInResponse>
        var status = 0
        try {
            response = authService.login(
                header.toString(),
                signInForm.username,
                signInForm.password,
                signInForm.grantType
            ).execute()

            body = response.body()

            // Set tokens
            accessToken = body?.accessToken
            refreshToken = body?.refreshToken

            with(sharedPref.edit()) {
                putString(getString(R.string.access_token_key), "Bearer $accessToken")
                putString(getString(R.string.refresh_token_key), refreshToken)
                apply()
            }
            status = response.code()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // If successfully logged in, go to home screen
        if (status == 200) {
            errorText = null
            updateUI(true)
        } else {
            // TODO: localisation
            errorText = "Incorrect username or password"
        }
    }

    private fun facebookLogin() {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    Log.d("LoginActivity", "Facebook token: " + loginResult.accessToken.token)
                    updateUI(true)
                }

                override fun onCancel() {
                    Log.d("LoginActivity", "Facebook onCancel.")
                }

                override fun onError(error: FacebookException) {
                    Log.d("LoginActivity", "Facebook onError.")
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (facebook) {
            callbackManager?.onActivityResult(requestCode, resultCode, data)
        }
        if (google) {
            if (requestCode == RC_SIGN_IN) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
        }
    }

    private fun googleSingIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            updateUI(true)
        } catch (e: ApiException) {
            Toast.makeText(this, "Google login ERROR " + e.statusCode, Toast.LENGTH_LONG).show()
            updateUI(false)
        }

    }

    private fun updateUI(isLogin: Boolean) {
        if (isLogin) {
            startActivity(Intent(applicationContext, HomeActivity::class.java))
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.d("be.kdg", "onConnectionFailed:$connectionResult")
    }
}
