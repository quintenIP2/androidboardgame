package be.kdg.boardgame.authentication.dto

data class SignInForm(
    val username: String,
    val password: String,
    val grantType: String = "password"
)