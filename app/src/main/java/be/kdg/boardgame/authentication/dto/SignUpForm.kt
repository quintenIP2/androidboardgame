package be.kdg.boardgame.authentication.dto

data class SignUpForm(
    var username: String = "",
    var email: String = "",
    var password: String = ""
)