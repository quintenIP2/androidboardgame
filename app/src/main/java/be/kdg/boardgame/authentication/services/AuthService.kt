package be.kdg.boardgame.authentication.services

import be.kdg.boardgame.authentication.dto.SignInResponse
import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.authentication.dto.SignUpResponse
import be.kdg.boardgame.gamecreation.dto.User
import retrofit2.Call
import retrofit2.http.*

interface AuthService {
    @POST("api/register")
    fun register(@Body signUpForm: SignUpForm): Call<SignUpResponse>

    @POST("oauth/token")
    @FormUrlEncoded
    fun login(
        @Header("Authorization") signInHeader: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("grant_type") grantType: String
    ): Call<SignInResponse>

    @GET("api/user/")
    fun getUserDetails(
        @Header("Authorization") signInHeader: String
    ): Call<User>
}