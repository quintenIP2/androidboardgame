package be.kdg.boardgame.friends.dto

data class Friend (
    var username: String = "",
    var email: String = "",
    var avatar: String = "",
    var activated:Boolean = false
)