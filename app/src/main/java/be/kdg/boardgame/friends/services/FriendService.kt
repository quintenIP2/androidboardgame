package be.kdg.boardgame.friends.services

import be.kdg.boardgame.addfriends.dto.AddFriendResponse
import be.kdg.boardgame.addfriends.dto.FriendDto
import be.kdg.boardgame.friends.dto.Friend
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface FriendService {

    @GET("api/friends")
    fun friends(
        @Header("Authorization") authHeader: String
    ): Call<List<Friend>>

    @POST("api/friends")
    fun addFriend(
        @Header("Authorization") authHeader: String,
        @Body friend:FriendDto
    ): Call<AddFriendResponse>

}