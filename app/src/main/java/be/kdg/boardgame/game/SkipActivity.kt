package be.kdg.boardgame.game

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import be.kdg.boardgame.R

class SkipActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_skip_options_layout)
    }
}