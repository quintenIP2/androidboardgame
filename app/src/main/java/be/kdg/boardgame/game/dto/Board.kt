package be.kdg.boardgame.game.dto

data class Board(var title: String?,
                 var board: String?,
                 var currentlyPlayng: Int?,
                 var gameOver: Boolean?)