package be.kdg.boardgame.game.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class Invitation(
    @JsonProperty
    val hasAccepted: Boolean = false,
    @JsonProperty
    val user: String = ""
)
