package be.kdg.boardgame.game.dto

data class LetterRack(
    val player: String = "",
    val game: String? = "",
    val tiles: List<LetterRackTile> = listOf()
)