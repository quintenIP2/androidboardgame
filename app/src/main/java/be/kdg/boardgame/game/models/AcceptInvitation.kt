package be.kdg.boardgame.game.models

data class AcceptInvitation(val hasAccepted: Boolean = false)
