package be.kdg.boardgame.game.services

import be.kdg.boardgame.game.dto.Invitation
import be.kdg.boardgame.game.models.AcceptInvitation
import be.kdg.boardgame.game.dto.PlaceTilesRequest
import be.kdg.boardgame.game.dto.PlayedTurn
import be.kdg.boardgame.gamecreation.dto.Game
import retrofit2.Call
import retrofit2.http.*


interface GameService {
    @POST("api/games/{gameId}/tiles")
    fun placeTiles(
        @Header("Authorization") signInHeader: String,
        @Path("gameId") gameName: String,
        @Body placeTilesRequest: PlaceTilesRequest
    ): Call<PlayedTurn>

    @POST("api/games/{gameId}")
    fun startGame(
        @Header("Authorization") signInHeader: String,
        @Path("gameId") gameName: String
    ): Call<Game>
    @GET("api/games/{gameId}")
    fun getGameById(
        @Header("Authorization") signInHeader: String,
        @Path("gameId") gameName: String
    ):Call<Game>

    @POST("api/games/{gameId}/invitations")
    fun handleInvitationResponse(
        @Header("Authorization") signInHeader: String,
        @Path("gameId") gameId: String,
        @Body inv: AcceptInvitation
    ): Call<Invitation>
}
