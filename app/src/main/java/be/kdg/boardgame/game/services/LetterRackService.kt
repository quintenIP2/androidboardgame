package be.kdg.boardgame.game.services

import be.kdg.boardgame.game.dto.DrawTilesRequest
import be.kdg.boardgame.game.dto.LetterRack
import be.kdg.boardgame.game.dto.PlayedTurn
import retrofit2.Call
import retrofit2.http.*


interface LetterRackService {
    @GET("api/letterracks/{gameId}")
    fun getLetterRack(
        @Header("Authorization") signInHeader: String,
        @Path("gameId") gameName: String
    ): Call<LetterRack>

    @POST("api/letterracks/{gameId}/tiles/random")
    fun getRandomTiles(
        @Header("Authorization") signInHeader: String,
        @Path("gameId") gameId: String,
        @Body drawTilesRequest: DrawTilesRequest
        ): Call<LetterRack>
}

