package be.kdg.boardgame.gamecreation.dto

data class GameRequest(
    val title: String,
    val language: String,
    val players: MutableList<String>
)