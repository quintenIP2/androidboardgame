package be.kdg.boardgame.gamecreation.dto

data class User(
    var username: String = "",
    var email: String = "",
    var avatar: String = "",
    var activated: Boolean = false
)