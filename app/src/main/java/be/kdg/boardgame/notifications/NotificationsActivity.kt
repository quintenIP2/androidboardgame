package be.kdg.boardgame.notifications

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import be.kdg.boardgame.BaseNavigationActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.notifications.adapters.NotificationAdapter
import be.kdg.boardgame.notifications.dto.Notification
import be.kdg.boardgame.notifications.services.NotificationService
import kotlinx.android.synthetic.main.activity_notifications.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


class NotificationsActivity : BaseNavigationActivity() {

    private var notifications = mutableListOf<Notification>()
    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private var accessToken: String? = null
    private lateinit var adapter: NotificationAdapter
    private lateinit var notificationService: NotificationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(be.kdg.boardgame.R.layout.activity_notifications)
        initBottomNavBar(be.kdg.boardgame.R.id.action_notifications, bottom_navigation, this)
        initConstants()
        adapter = NotificationAdapter(this, notifications, game_notifications_listview)
        initRecyclerView()

        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(R.string.access_token_key), null)

        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.Default) {
                val bearer = accessToken.toString()
                val response = notificationService.notifications(bearer).execute()
                val notifs = response.body() ?: emptyList()

                response.errorBody()
                notifications.addAll(notifs)

                if (response.isSuccessful)
                else if (response.code() == 404)
                else throw HttpException(response)
            }

            Log.d("INFO", notifications.toString())
            Log.d("INFO", accessToken)
            initRecyclerView()
        }

        onRefresh()
    }

    private fun initRecyclerView() {
        val recyclerView: RecyclerView = findViewById(R.id.game_notifications_listview)
        adapter.notifications = notifications.sortedWith(compareBy { it.date }).toMutableList()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        notificationService = retrofit.create(NotificationService::class.java)
    }

    private fun onRefresh() {
        swiperefreshnotifications.setOnRefreshListener {
            GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.Default) {
                    val bearer = accessToken.toString()
                    val response = notificationService.notifications(bearer).execute()
                    val notifs = response.body() ?: emptyList()
                    notifications.removeAll(notifications)
                    notifications.addAll(notifs)

                    if (response.isSuccessful)
                    else if (response.code() == 404)
                    else throw HttpException(response)
                }

                Log.d("INFO", notifications.toString())
                Log.d("INFO", accessToken)
                initRecyclerView()
            }

            swiperefreshnotifications.isRefreshing = false
            adapter.notifyDataSetChanged()
        }

    }

}