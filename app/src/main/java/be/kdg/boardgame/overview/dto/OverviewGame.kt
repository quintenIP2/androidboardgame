package be.kdg.boardgame.overview.dto

import be.kdg.boardgame.gamecreation.dto.User
import com.fasterxml.jackson.annotation.JsonProperty

data class OverviewGame(
    @JsonProperty
    val id: String = "",
    @JsonProperty
    val title: String = "",
    @JsonProperty
    val currentlyPlaying: String = "", // error
    @JsonProperty
    val players: MutableList<User> = mutableListOf(),
    @JsonProperty
    val creator: User = User(),
    @JsonProperty
    val over: Boolean = false,
    @JsonProperty
    val hasStarted: Boolean = false,
    // TODO: image snapshot,
    @JsonProperty
    val winner: String = "",
    @JsonProperty
    val turns: Int = 0
)